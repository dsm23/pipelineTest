wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u152-b16/aa0333dd3019491ca4f6ddbe78cdb6d0/jdk-8u152-linux-x64.rpm"

wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u152-b16/aa0333dd3019491ca4f6ddbe78cdb6d0/jre-8u152-linux-x64.tar.gz"

sudo cp -avr jdk1.8.0_152 jre1.8.0_152 /opt

sudo update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_152/bin/java 100

sudo update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_152/bin/java 100

sudo update-alternatives --install /usr/bin/java java /opt/jre1.8.0_152/bin/java 100

sudo apt install maven

